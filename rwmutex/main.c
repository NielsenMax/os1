#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include "rwmutexA.h"

#define M 15
#define N 5
#define ARRLEN 10240

struct rwmutex_t lock;

int arr[ARRLEN];
void * escritor(void *arg){ 
	int i;
	int num = arg - (void*)0;
	while (1) {
    sleep(random() % 3);
    rwLock(&lock);
		printf("Escritor %d escribiendo\n", num);
    for (i = 0; i < ARRLEN; i++)
      arr[i] = num;
    rwUnlock(&lock);
	}
	return NULL;
}
void * lector(void *arg){
	int v, i;
	int num = arg - (void*)0;
	while (1) {
		sleep(random() % 3);
    rLock(&lock);
		v = arr[0];
		for (i = 1; i < ARRLEN; i++) {
			if (arr[i] != v)
				break;
		}
		if (i < ARRLEN)
			printf("Lector %d, error de lectura\n", num);
		else
			printf("Lector %d, dato %d\n", num, v);
    rUnlock(&lock);
	}
	return NULL;
}
int main(){
	pthread_t lectores[M], escritores[N];
  rwmutex_init(&lock);
	int i;
	for (i = 0; i < M; i++)
		pthread_create(&lectores[i], NULL, lector, i + (void*)0);
	for (i = 0; i < N; i++)
		pthread_create(&escritores[i], NULL, escritor, i + (void*)0);
	pthread_join(lectores[0], NULL);
	/* Espera para siempre */
	return 0;
}
