#ifndef __RWMUTEX_H__
#define __RWMUTEX_H__
#include <pthread.h>
#include <semaphore.h>

struct rwmutex_t {
	sem_t writeSem;               // Semaforo usado para indicar si se puede escribir
  pthread_mutex_t structMutex;  // Mutex para las varibles de la estructura
  pthread_cond_t noWriters;     // Se usa para que los lectores esperen que ya no haya escritores
	int readers;
  int writers;
};

void rwmutex_init(struct rwmutex_t*);

void rLock(struct rwmutex_t*);

void rUnlock(struct rwmutex_t*);

void rwLock(struct rwmutex_t*);

void rwUnlock(struct rwmutex_t*);

#endif // !
