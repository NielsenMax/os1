#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "rwmutexA.h"


void rwmutex_init(struct rwmutex_t *l){
  pthread_mutex_init(&l->readersMutex, NULL);
  sem_init(&l->writeSem, 0, 1);
  l->readers = 0;  
}

void rLock(struct rwmutex_t *l){
  pthread_mutex_lock(&l->readersMutex);
	if(l->readers == 0){
    sem_wait(&l->writeSem);
    l->readers++;		
	} else {
    l->readers++;
  }
  pthread_mutex_unlock(&l->readersMutex);  
}

void rUnlock(struct rwmutex_t *l){
  pthread_mutex_lock(&l->readersMutex);
  if(l->readers > 0){
    l->readers--;
    if(l->readers == 0){
      sem_post(&l->writeSem);
    }
  }  
  pthread_mutex_unlock(&l->readersMutex);
}

void rwLock(struct rwmutex_t *l){
  sem_wait(&l->writeSem);
}

void rwUnlock(struct rwmutex_t *l){
  sem_post(&l->writeSem);  
}

