#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include "rwmutexB.h"

void rwmutex_init(struct rwmutex_t *l){
  pthread_mutex_init(&l->structMutex, NULL);
  pthread_cond_init(&l->noWriters, NULL);
  sem_init(&l->writeSem, 0, 1);
  l->readers = 0;
  l->writers = 0;
}

void rLock(struct rwmutex_t *l){
  pthread_mutex_lock(&l->structMutex);
	while (l->writers > 0) {
	  pthread_cond_wait(&l->noWriters, &l->structMutex);
	}
  if (l->readers == 0) {
    l->readers++;
    sem_wait(&l->writeSem);
  } else {
    l->readers++;
  }
  pthread_mutex_unlock(&l->structMutex);  
}

void rUnlock(struct rwmutex_t *l){
  pthread_mutex_lock(&l->structMutex);
  if(l->readers > 0){
    l->readers--;
    if(l->readers == 0){
      sem_post(&l->writeSem);
    }
  }  
  pthread_mutex_unlock(&l->structMutex);
}

void rwLock(struct rwmutex_t *l){
  pthread_mutex_lock(&l->structMutex);
  l->writers++;
  pthread_mutex_unlock(&l->structMutex);
  sem_wait(&l->writeSem);
}

void rwUnlock(struct rwmutex_t *l){
  pthread_mutex_lock(&l->structMutex);
  l->writers--;
  if (l->writers == 0) {
    pthread_cond_broadcast(&l->noWriters);
  }
  pthread_mutex_unlock(&l->structMutex);
  sem_post(&l->writeSem);  
}
