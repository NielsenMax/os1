#include "rwmutexC.h"
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

static void pushNode (struct rwmutex_t *l){
  Node *new = malloc(sizeof(Node));
  new->prev = NULL;
  new->next = l->fst;
  new->readers = 0;
  new->shouldRead = 0;
  pthread_cond_init(&new->wait, NULL);
  if (l->fst) {
    l->fst->prev = new;
  } else {
    l->lst = new;
  }
  l->fst = new;
}

static void popNode(struct rwmutex_t *l) {
  if (l->lst) {
    Node *aux = l->lst->prev;
    pthread_cond_destroy(&l->lst->wait);
    free(l->lst);
    l->lst = aux;
    if (l->lst) {
      l->lst->next = NULL;
    } else {
      l->fst = NULL;
    }
  }
}

void rwmutex_init(struct rwmutex_t *l) {
  l->fst = NULL;
  l->lst = NULL;
  pthread_mutex_init(&l->structLock, NULL);
}

void rLock(struct rwmutex_t *l) {
  pthread_mutex_lock(&l->structLock);
  if (l->fst != NULL) {
    if (l->fst->readers > 0) {
      l->fst->readers++;
      if (l->fst->shouldRead == 0) {
        pthread_cond_wait(&l->fst->wait, &l->structLock);
      }
    } else {
      pushNode(l);
      l->fst->readers++;
      pthread_cond_wait(&l->fst->wait, &l->structLock);
    } 
  } else {
    pushNode(l);
    l->fst->readers++;
    l->fst->shouldRead++;
  }
  pthread_mutex_unlock(&l->structLock);
}

void rUnlock(struct rwmutex_t *l) {
  pthread_mutex_lock(&l->structLock);
  l->lst->readers--;
  if (l->lst->readers == 0) {
    popNode(l);
    if (l->lst) {
      l->lst->shouldRead++;
      pthread_cond_broadcast(&l->lst->wait);
    }
  }
  pthread_mutex_unlock(&l->structLock);
}

void rwLock(struct rwmutex_t *l) {
  pthread_mutex_lock(&l->structLock);
  if (l->fst) {
    pushNode(l);
    pthread_cond_wait(&l->fst->wait, &l->structLock);
  } else {
    pushNode(l);
  }
  pthread_mutex_unlock(&l->structLock);
}

void rwUnlock(struct rwmutex_t *l) {
  pthread_mutex_lock(&l->structLock);
  popNode(l);
  if (l->lst) {
    l->lst->shouldRead++;
    pthread_cond_broadcast(&l->lst->wait);
  }
  pthread_mutex_unlock(&l->structLock);
}
