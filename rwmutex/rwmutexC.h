#ifndef __RWMUTEX_H__
#define __RWMUTEX_H__
#include <pthread.h>

/**
 * La idea principal sera tener una lista doblemente enlazada con una variable de condicion
 * para esperar si es necesario. Luego cuando un nodo se libere este hara un broadcast sobre
 * esta variable.
 **/

typedef struct _Node{
  pthread_cond_t wait;  // Se usa para poder esperar si es no es mi turno
  int readers;          // Cantidad de lectores esperando o leyendo en este nodo
  int shouldRead;       // Indica si al llegar un nuevo lector al nodo debe esperar la condicion o no
  struct _Node *prev;
  struct _Node *next;
} Node;

struct rwmutex_t {
  Node *fst;
  Node *lst;
  pthread_mutex_t structLock;
}; 

void rwmutex_init(struct rwmutex_t*);

void rLock(struct rwmutex_t*);

void rUnlock(struct rwmutex_t*);

void rwLock(struct rwmutex_t*);

void rwUnlock(struct rwmutex_t*);

#endif // !
