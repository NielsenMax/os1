#include <stdlib.h>
#include "lamport.h"

void init(struct lamport *l, int n) { 
  l->MAX_THREADS = n;
  l->tickets = malloc(sizeof(int) * n);
  l->choosing = malloc(sizeof(int) * n);
} 

void destroy(struct lamport *l) {
  free(l->tickets);
  free(l->choosing);
}

void lock(struct lamport *lamp, int l) {
  lamp->choosing[l] = 1;
  asm("mfence"); // antes de avanzar, me aseguro que todos sean que estoy eligiendo
  int max = 0;
  for (int i = 0; i < lamp->MAX_THREADS; i++) {
    if (max < lamp->tickets[i]) {
      max = lamp->tickets[i];    
    }
  } 
  lamp->tickets[l] = max + 1;
  asm("mfence"); // antes de señalizar que no estoy eligiendo, me aseguro que todos sepan lo que elegi
  lamp->choosing[l] = 0;
  asm("mfence"); // me aseguro que todos sepan que no estoy eligiendo
  for (int j = 0; j < lamp->MAX_THREADS; j++) {
    while (lamp->choosing[j]);
    asm("mfence"); // no se
    while ((lamp->tickets[j] != 0) && ((lamp->tickets[j] < lamp->tickets[l]) || ((lamp->tickets[j]==lamp->tickets[l]) && (j < l))));   
  } 
}

void unlock(struct lamport *lamp, int l) {
  lamp->tickets[l] = 0;
}

