#ifndef __LAMPORT_H__
#define __LAMPORT_H__

struct lamport {
  int *tickets;
  int *choosing;
  int MAX_THREADS;
};

void init(struct lamport *, int);  

void destroy(struct lamport *);

void lock(struct lamport *, int); 

void unlock(struct lamport *, int);

#endif
