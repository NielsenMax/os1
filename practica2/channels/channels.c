#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include "channels.h"

void channel_init(struct channel *c) {
  pthread_mutex_init(&c->structLock, NULL);
  sem_init(&c->writer, 0, 0);
  sem_init(&c->reader, 0, 0);
  c->msg = -1;
}

void chan_write(struct channel *c, int v) {
  sem_wait(&c->reader);
  c->msg = v;
  sem_post(&c->writer);
}

int chan_read(struct channel *c) {
  int returnValue;
  pthread_mutex_lock(&c->structLock);
  sem_post(&c->reader);
  sem_wait(&c->writer);
  returnValue= c->msg;
  c->msg = -1;
  pthread_mutex_unlock(&c->structLock);
  return returnValue;
}

void chan_destroy(struct channel *c) {
  pthread_mutex_destroy(&c->structLock);
  sem_destroy(&c->reader);
  sem_destroy(&c->writer);
}
