#ifndef __CHANNELS__
#define __CHANNELS__
#include <pthread.h>
#include <semaphore.h>

struct channel {
  int msg;
  pthread_mutex_t structLock;
  sem_t writer, reader;
};

void channel_init(struct channel *); 

void chan_write(struct channel *, int); 

int chan_read(struct channel *); 

void chan_destroy(struct channel *);

#endif // !__CHANNELS__
