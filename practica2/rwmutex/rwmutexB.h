#ifndef __RWMUTEX_H__
#define __RWMUTEX_H__
#include <pthread.h>
#include <semaphore.h>

/**
 *  La idea la misma que en el apartado A solo que ahora si hay escritores esperando
 *  los lectores esperaran la condicion noWriters. Y el ultimo escritor realiza un broadcast sobre esta
 **/

struct rwmutex_t {
	sem_t writeSem;               // Semaforo usado para indicar si se puede escribir
  pthread_mutex_t structMutex;  // Mutex para las varibles de la estructura
  pthread_cond_t noWriters;     // Se usa para que los lectores esperen que ya no haya escritores
	int readers;
  int writers;
};

void rwmutex_init(struct rwmutex_t*);

void rLock(struct rwmutex_t*);

void rUnlock(struct rwmutex_t*);

void rwLock(struct rwmutex_t*);

void rwUnlock(struct rwmutex_t*);

void rwDestroy(struct rwmutex_t *);

#endif // !
