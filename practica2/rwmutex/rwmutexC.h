#ifndef __RWMUTEX_H__
#define __RWMUTEX_H__
#include <pthread.h>

/**
 * La idea principal sera tener una lista doblemente enlazada donde cada nodo
 * tiene una variable de condicion, una bandera y un contador de lectores.
 * La lista tendra dos "tipos" de nodos, uno para escritores y otro para lectores.
 * Los de escritores tendran la variable readers en 0 y solo les importara la
 * variable de condicion wait.
 * Los lectores tendran la variable readers con un valor mayor a cero. Esta variable indica
 * cuantos lectores estan en este nodo, ya sea leyendo o esperando. Para que un nuevo lector sepa
 * si tiene que comenzar a leer o esperar la variable de condicion, comprobara la bandera shouldRead.
 * El ultimo lector o el escritor realizara un broadcast sobre la variable de condicion
 * de su nodo previo, si es que este existe, para despertar a los threads que la esten esperando.
 **/

typedef struct _Node{
  pthread_cond_t wait;  // Se usa para poder esperar si no es mi turno
  int readers;          // Cantidad de lectores esperando o leyendo en este nodo
  int shouldRead;       // Indica si al llegar un nuevo lector al nodo debe esperar la condicion o no
  struct _Node *prev;
  struct _Node *next;
} Node;

struct rwmutex_t {
  Node *fst;
  Node *lst;
  pthread_mutex_t structLock;
}; 

void rwmutex_init(struct rwmutex_t*);

void rLock(struct rwmutex_t*);

void rUnlock(struct rwmutex_t*);

void rwLock(struct rwmutex_t*);

void rwUnlock(struct rwmutex_t*);

void rwDestroy(struct rwmutex_t *);

#endif // !
