#ifndef __RWMUTEX_H__
#define __RWMUTEX_H__
#include <pthread.h>
#include <semaphore.h>

/**
 *  La idea es que el primer lector le hara un wait al semaforo writeSem y
 *  y el ultimo en salir le hara un post. El escritor solo usa este semaforo
 **/

struct rwmutex_t {
	sem_t writeSem;                 // Semaforo usado para indicar si se puede escribir o no
  pthread_mutex_t readersMutex;   // Mutex usado para cambiar la variable readers
	int readers;	                  // Cantidad de lectores actualmente
};

void rwmutex_init(struct rwmutex_t*);

void rLock(struct rwmutex_t*);

void rUnlock(struct rwmutex_t*);

void rwLock(struct rwmutex_t*);

void rwUnlock(struct rwmutex_t*);

void rwDestroy(struct rwmutex_t *);

#endif // !
