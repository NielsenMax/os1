#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define NUM_WORKERS 100
#define WORK_LOAD 1000

pthread_t tid[NUM_WORKERS];
int outsideOfCircle;
pthread_mutex_t lock;

void *calcPoint(void *arg)
{
  float x, y;
  for (int i = 0; i < WORK_LOAD; i++)
  {
    x = ((float)rand() / (float)(RAND_MAX)) * 2 - 1;
    y = ((float)rand() / (float)(RAND_MAX)) * 2 - 1;
    // Only entering a critical section when the points fall outside the circle is better
    // because the area outside is smaller, so it's faster
    if ((x * x + y * y) > 1)
    {
      pthread_mutex_lock(&lock);
      outsideOfCircle++;
      pthread_mutex_unlock(&lock);
    }
  }
}

int main()
{
  int err;
  srand(time(0));
  if (pthread_mutex_init(&lock, NULL) != 0)
  {
    printf("The mutex init failed\n");
    return 1;
  }
  for (int i = 0; i < NUM_WORKERS; i++)
  {
    err = pthread_create(&(tid[i]), NULL, &calcPoint, NULL);
    if (err != 0)
    {
      printf("Can't create thread: %s\n", strerror(err));
    }
  }
  for (int i = 0; i < NUM_WORKERS; i++)
  {
    pthread_join(tid[i], NULL);
  }
  int maxPoints = NUM_WORKERS * WORK_LOAD;
  printf("The result is: %f \n", (float)((4 * (float)(maxPoints - outsideOfCircle)) / maxPoints));
  pthread_mutex_destroy(&lock);
  return 0;
}