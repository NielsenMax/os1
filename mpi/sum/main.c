#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define N 1003

int arr[N];

int main(int argc, char** argv){
  MPI_Init(&argc, &argv);
  int process_Rank, size_Of_Comm;
  MPI_Comm_size(MPI_COMM_WORLD, &size_Of_Comm);
  MPI_Comm_rank(MPI_COMM_WORLD, &process_Rank);
  int M = N/size_Of_Comm;
  int scattered_Data[M];
  int gather[size_Of_Comm];
  int localSum = 0;
  int global = 0;

  // El proceso inicial inicializa el array
  if (process_Rank == 0)
  {
    for(int i= 0; i < N; ++i)
    {
      arr[i] = i;
    }
    // Suma a su variable privada los elementos que no entren en el scatter
    for(int i = M*size_Of_Comm; i < N; i++) {
      localSum += arr[i];
    }
  }
  // Distribuye los elementos
  MPI_Scatter(&arr, M, MPI_INT, &scattered_Data, M, MPI_INT, 0, MPI_COMM_WORLD);
  
  // Cada uno suma lo que recibe
  for(int i= 0; i < M; ++i )
    localSum += scattered_Data[i];
  
  // Se reduce con el operador de suma las variables privadas
  MPI_Reduce(&localSum, &global, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  
  if (process_Rank == 0) {
    int result = N*(N-1)/2; // Suma del 0 al N-1
    printf("The reduccion result is %i, and is expected to be  %i\n", global, result);
  }

  MPI_Finalize();
  return 0;
}
