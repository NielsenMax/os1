#ifndef __CHANNELS__
#define __CHANNELS__
#include <pthread.h>
#include <semaphore.h>

struct channel {
  int msg;
  pthread_mutex_t structLock;
  sem_t writer, reader;
};

#endif // !__CHANNELS__
