#include <stdlib.h>
#include <stdio.h>
#include <semaphore.h>
#include <unistd.h>
#include <pthread.h>

#include "channels.h"

#define N 10

struct channel channel;

void * escritor(void *arg){ 
	int num = arg - (void*)0;
	for (int i = 0; 1; i++) {
    sleep(random() % 3);
    int msg = random();
    chan_write(&channel, msg);
    printf("Escritor %d escribiendo %d\n", num, msg);
	}
	return NULL;
}

void * lector(void *arg){
	int lastRead = -1, i;
	int num = arg - (void*)0;
	while (1) {
		sleep(random() % 3);
    i = chan_read(&channel);
    printf("Lector %d, leyo %d\n", num, i);
    /**
    if (lastRead == i) {
      printf("El lector %d leyo dos veces el valor %d\n", num, i);
    } else if ((lastRead+1) != i) {
      printf("El lector %d leyo el valor %d pero esperada %d\n", num, i, (lastRead+1));
    }
    **/
    lastRead = i;
  }
	return NULL;
}
int main(){
	pthread_t lectores[N], escritores[N];
	int i;
//	for (i = 0; i < N; i++) {
    channel_init(&channel);
 // }
	for (i = 0; i < N; i++) {
		pthread_create(&escritores[i], NULL, escritor, i + (void*)0);
    pthread_create(&lectores[i], NULL, lector, i + (void*)0);
  }
	pthread_join(lectores[0], NULL);
	/* Espera para siempre */
	return 0;
}
