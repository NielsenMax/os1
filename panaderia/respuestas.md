Este algortimo necesita tener en memoria 2 arrays de ints con
un largo de la maxima cantidad de threads que se esten usando.
Esto es aproximadamente (CANT_THREADS * 8) Bytes.
Si comparamos esto con la solucion de Peterson para N procesos,
podemos ver que este tambien necesita una cantidad de memoria
similar ya que posee dos arrays de ints de un largo similar.

Por lo tanto la solucion resulta similar en cantidad de memoria.

A la hora de crear una libreria el problema que se tiene es que se tiene
que especificar que thread esta llamando al lock y al unlock.
Por lo que se debe conocer un poco de la implementacion y no
podemos abstraernos al mismo nivel que cuando usamos mutex
de pthreads.
