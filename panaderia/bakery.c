#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "lamport.h"


#define MAX_THREADS 4
#define N_VISITANTES 1200000

struct lamport l;

int visitantes = 0;

void *molinete(void *arg) {
  for (int i = 0; i < N_VISITANTES / MAX_THREADS; i++){
    lock(&l, arg - NULL);
    visitantes++;
    //if ((visitantes & 0xff) == 0) printf("LLEGA EL VISITANTE NUMERO %d\n", visitantes);
    unlock(&l, arg - NULL);
  }
  return NULL;
}

int main(){      
  pthread_t tids[MAX_THREADS];
  init(&l, MAX_THREADS);
  for (int i = 0; i < MAX_THREADS; ++i)
    pthread_create(&tids[i], NULL, molinete, NULL + i);

  for (int i = 0; i < MAX_THREADS; ++i)
    pthread_join(tids[i], NULL);
   
  destroy(&l);
  printf("Hoy hubo %d visitantes!\n", visitantes);
  return 0;
}
