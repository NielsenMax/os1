-module(sync).
-export([createLock/0, lock/1, unlock/1, destroyLock/1, lock_loop/1]).
-export([createSem/1, semP/1, semV/1, destroySem/1, sem_loop/2]).
-export([testLock/0, testSem/0]).

lock_loop(Ps) ->
  receive
    { lock, Pid } -> if
                       Ps == [] -> Pid ! unlock, lock_loop([Pid]);
                       true -> lock_loop([Pid | Ps])
                     end;
    unlock -> case Ps of
                [] -> lock_loop(Ps);
                [Pid] -> Pid ! unlock, lock_loop([]);
                _ -> Tail = lists:droplast(Ps),
                     lists:last(Tail) ! unlock,
                     lock_loop(Tail)
              end;
    destroy -> lists:foreach(fun (Pid) -> Pid ! unlock end, Ps), ok;
    _ -> lock_loop(Ps)
  end.

wait() -> receive unlock -> ok end.

createLock () -> spawn(?MODULE, lock_loop, [[]]).
lock (L) -> L ! {lock, self()}, wait().
unlock (L) -> L ! unlock.
destroyLock (L) -> L ! destroy.

sem_loop(N, Ps) ->
  receive
    { wait, Pid } -> if
                       N == 0 -> sem_loop(N, [Pid | Ps]);
                       true -> Pid ! unlock , sem_loop(N-1, Ps)
                     end;
    post -> if
              Ps == [] -> sem_loop(N+1, Ps);
              true -> lists:last(Ps) ! unlock, sem_loop(N, lists:droplast(Ps))
            end;
    destroy -> lists:foreach(fun (Pid) -> Pid ! unlock end, Ps), ok;
    _ -> sem_loop(N, Ps)
  end.


createSem (N) -> spawn(?MODULE, sem_loop, [N,[]]).
destroySem (S) -> S ! destroy.
semP (S) -> S ! { wait, self() }, wait().
semV (S) -> S ! post.

f (L, W) ->
	lock(L),
	% regioncritica(),
	io:format("uno ~p~n", [self()]),
	io:format("dos ~p~n", [self()]),
	io:format("tre ~p~n", [self()]),
	io:format("cua ~p~n", [self()]),
	unlock(L),
	W ! finished.

waiter (L, 0) -> destroyLock(L);
waiter (L, N) -> receive finished -> waiter(L, N-1) end.

waiter_sem (S, 0) -> destroySem(S);
waiter_sem (S, N) -> receive finished -> waiter_sem(S, N-1) end.

testLock () ->
	L = createLock(),
	W = spawn(fun () -> waiter(L, 3) end),
	spawn (fun () -> f(L, W) end),
	spawn (fun () -> f(L, W) end),
	spawn (fun () -> f(L, W) end),
	ok.

sem (S, W) ->
	semP(S),
	%regioncritica(), bueno, casi....
	io:format("uno ~p~n", [self()]),
	io:format("dos ~p~n", [self()]),
	io:format("tre ~p~n", [self()]),
	io:format("cua ~p~n", [self()]),
	io:format("cin ~p~n", [self()]),
	io:format("sei ~p~n", [self()]),
	semV(S),
	W ! finished.

testSem () ->
	S = createSem(2), % a lo sumo dos usando io al mismo tiempo
	W = spawn (fun () -> waiter_sem (S, 5) end),
	spawn (fun () -> sem (S, W) end),
	spawn (fun () -> sem (S, W) end),
	spawn (fun () -> sem (S, W) end),
	spawn (fun () -> sem (S, W) end),
	spawn (fun () -> sem (S, W) end),
	ok.
