-module(broadcaster).
-export([subscribe/0, unsubscribe/0, send/1, server_start/0, server_end/0, loop/1, client/0, spawn_client/0]).

subscribe() ->
  broadcaster ! { subscribe, self() }, ok.

send(Msg) ->
  broadcaster ! { send, Msg }, ok.

unsubscribe() ->
  broadcaster ! { unsubscribe, self() }, ok.

broadcast(Msg, Ps) ->
  lists:foreach(fun(Pid) -> Pid ! Msg end, Ps), ok.

loop(Ps) ->
  receive
    { subscribe, Pid } -> case lists:member(Pid, Ps) of
                            false -> loop([Pid | Ps]);
                            true -> loop(Ps)
                          end;
    { unsubscribe, Pid } -> loop(lists:delete(Pid, Ps));
    { send, Msg } -> broadcast(Msg, Ps), loop(Ps);
     bye -> ok;
    _ -> loop(Ps)
  end.

server_start() ->
  io:fwrite("Starting broadcast server~n"),
  Pid = spawn(?MODULE, loop, [[]]),
  register(broadcaster, Pid),
  ok.

server_end() -> 
  broadcaster ! bye,
  unregister(broadcaster),
  ok.

% This functions are for testing

client_loop() ->
  receive
    0 -> io:fwrite("Im ~p and im going to unsubscribe~n", [self()]);
    N -> io:fwrite("Im ~p and i receive ~p ~n", [self(), N]), client_loop()
  end.

client() ->
  subscribe(),
  subscribe(),
  client_loop(),
  unsubscribe(),
  ok.

spawn_client() ->
  spawn(?MODULE, client, []).
