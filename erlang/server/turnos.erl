-module(turnos).
-export([server/0, number_handler/1]).

number_handler(N) ->
	receive
		{new, Pid} -> Pid ! N+1,
					number_handler(N+1);
		_ -> number_handler(N)
	end. 

server() ->
	Num_h =  spawn(?MODULE, number_handler, [0]),
	register(num_h, Num_h),
	{ok, ListenSocket} = gen_tcp:listen(8000, [{reuseaddr, true}, 
                                        {active, false}]),
	wait_connect(ListenSocket, 0).

wait_connect(ListenSocket, N) ->
	{ok, Socket} = gen_tcp:accept(ListenSocket),
	spawn (fun () -> wait_connect (ListenSocket, N+1) end),
	get_request(Socket, "").

get_request (Socket, Bs) ->
  case gen_tcp:recv(Socket, 0) of
    {ok, B} -> {NewS, NewBs} = parser("", Bs ++ B),
               case process_request(Socket, NewS, NewBs) of
                 close -> gen_tcp:close(Socket);
                 AfterBs -> get_request(Socket, AfterBs)
               end;
    {error, _} -> io:fwrite("The connection end abruptly~n")
  end.

process_request (_, "", Bs) -> Bs;
process_request (Socket, S, Bs) ->
  matcher(self(), S),
  receive
    continue -> {NewS, NewBs} = parser("", Bs),
                process_request (Socket, NewS, NewBs);
    close -> close;
    N -> gen_tcp:send(Socket, [integer_to_list(N) ++ "\n", 0]),
         {NewS, NewBs} = parser("", Bs),
         process_request (Socket, NewS, NewBs)
  end.


parser (S, "") -> {"", S};
parser (S, [X | Xs]) -> 
  case X of
    $\n -> {S, Xs};
    _    -> parser (S ++ [X], Xs)  
  end.

matcher (Server, S) ->
  case S of
    "NUEVO" -> num_h ! {new, Server}, ok;
    "CHAU"  -> Server ! close, ok;
    _ -> Server ! continue, ok
  end.
