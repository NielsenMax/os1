-module(hello).
-export([init/0, watcher/0, hello/0, watcher_loop/1]).
-on_load(reload/0).

hello () ->
  receive after 1000 -> ok end,
  io:fwrite("hola ~p~n", [case rand:uniform(10) of 10 -> 1/uno; _ -> self() end]),
  hello().

watcher () ->
  register(watcher, self()),
  process_flag(trap_exit, true),
  Pid = spawn_link(?MODULE, hello, []),
  watcher_loop(Pid).

watcher_loop (Pid) ->
  receive
    {'EXIT', Pid, _ } -> NewPid = spawn_link(?MODULE, hello, []), watcher_loop(NewPid);
    reload -> ?MODULE:watcher_loop(Pid);
    _ -> ok
  end.

reload () -> case whereis(watcher) of
               undefined -> ok;
               _ -> watcher ! reload, ok
             end.

init () -> spawn(?MODULE, watcher, []).
