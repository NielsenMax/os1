#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <pthread.h>

/*
 * Para probar, usar netcat. Ej:
 *
 *      $ nc localhost 4040
 *      NUEVO
 *      0
 *      NUEVO
 *      1
 *      CHAU
 */

void quit(char *s)
{
	perror(s);
	abort();
}

int U = 0;

pthread_mutex_t lock;

int fd_readline(int fd, char *buf)
{
	int rc;
	int i = 0;

	/*
	 * Leemos de a un caracter (no muy eficiente...) hasta
	 * completar una línea.
	 */
	while ((rc = read(fd, buf + i, 1)) > 0) {
		if (buf[i] == '\n')
			break;
		i++;
	}

	if (rc < 0)
		return rc;

	buf[i] = 0;
	return i;
}


fd_set set;


void responder(int csock){
	char buf[200];
	int rc;
  	rc = fd_readline(csock, buf);
	/* Atendemos pedidos, uno por linea */
	
	if (rc < 0)
		quit("read... raro");

	if (rc == 0) {
		/* linea vacia, se cerró la conexión */
		close( csock );
		FD_clear(csock, &set);
		return;
	}

	if (!strcmp(buf, "NUEVO")) {
		char reply[20]; 
		sprintf(reply, "%d\n", U);
		///pthread_mutex_lock(&lock);
		U++;
		///pthread_mutex_unlock(&lock);
		write(csock, reply, strlen(reply));
	} else if (!strcmp(buf, "CHAU")) {
		close(csock);
		FD_clear(csock, &set);
		return;
	}
	
}

void *manejar(void *a){
	
	printf("Mientras tanto estamos esperando a que llegue algo al select.\n");
	
	while(1){
		int dev = select(FD_SETSIZE, &set, NULL , NULL, NULL);
		printf("Tenemos %d procesos que necesitan que les respondamos.\n",dev);
		int i;
		for(i = 0; i < FD_SETSIZE; i++){
			if( FD_ISSET(i, &set) ){ /// respondemos a la conexion i
				responder(i);
			}
		}

	}
	printf("Dejamos de esperar al select.\n");
	
}

void wait_for_clients(int lsock){
	int csock;

	/* Esperamos una conexión, no nos interesa de donde viene */
	csock = accept(lsock, NULL, NULL);
	if (csock < 0)
		quit("accept");

	/* Atendemos al cliente */
	  
	 printf("Tenemos una conexion nueva, metemos a %d al set.\n",csock);
	 FD_SET(csock, &set);
	 pthread_t tid;
  	pthread_create( &tid , NULL , &manejar , NULL ); ///
	 

	//handle_conn(csock);

	/* Volvemos a esperar conexiones */
	wait_for_clients(lsock);

}

/* Crea un socket de escucha en puerto 4040 TCP */
int mk_lsock()
{
	struct sockaddr_in sa;
	int lsock;
	int rc;
	int yes = 1;

	/* Crear socket */
	lsock = socket(AF_INET, SOCK_STREAM, 0);
	if (lsock < 0)
		quit("socket");

	/* Setear opción reuseaddr... normalmente no es necesario */
	if (setsockopt(lsock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == 1)
		quit("setsockopt");

	sa.sin_family = AF_INET;
	sa.sin_port = htons(4040);
	sa.sin_addr.s_addr = htonl(INADDR_ANY);

	/* Bindear al puerto 4040 TCP, en todas las direcciones disponibles */
	rc = bind(lsock, (struct sockaddr *)&sa, sizeof sa);
	if (rc < 0)
		quit("bind");

	/* Setear en modo escucha */
	rc = listen(lsock, 10);
	if (rc < 0)
		quit("listen");

	return lsock;
}

int main()
{
	int lsock;
  if( pthread_mutex_init(&lock,NULL) != 0){
    printf("Fallo el inicio del mutex\n");
    return 1;
  }	
  	FD_ZERO(&set);
  
  	lsock = mk_lsock();
	wait_for_clients(lsock);
}


