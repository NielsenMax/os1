# Ejercicio 6

1. ¿Cómo puede ocurrir un deadlock?
Un deadlock puede si dos threads toman un recurso que necesita el otro al mismo tiempo.
Ejemplo, que el fumador 1 tome el tabaco y antes de que tome el papel, el fumador 3 lo tome.
2. Como se puede solucionar este deadlock
 Para solucionar esto implementamos 3 nuevos threads que su unica labor es consumir recursos
y contar cuantos recursos de ese tipo hay en la mesa. Cada vez que llega un nuevo recurso se usa
un broadcast sobre una variable de condicion para despertar a todos los fumadores para ver si alguno
cuenta con los recursos necesarios para fumar, sino vuelve a esperar esta variable condional.
