#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

sem_t tabaco, papel, fosforos, otra_vez;
int cantTabaco = 0, cantPapel = 0, cantFosforos = 0; // Cantidad de recursos en la mesa
pthread_mutex_t recursos; // Mutex para los recursos
pthread_cond_t recursoNuevo; // Variable condicional para despertar a los fumadores

void agente()
{
    for (;;) {
        int caso = random() % 3;
        sem_wait(&otra_vez);
        switch (caso) {
            case 0:
                sem_post(&tabaco);
                sem_post(&papel);
                break;
            case 1:
                sem_post(&fosforos);
                sem_post(&tabaco);
                break;
            case 2:
                sem_post(&papel);
                sem_post(&fosforos);
                break;
        }
    }
}

void fumar(int fumador)
{
    printf("Fumador %d: Puf! Puf! Puf!\n", fumador);
    sleep(0);
}

// Cada fumador solo se despierta cuando hay un nuevo recurso en la mesa
void *fumador1(void *arg)
{
  for (;;) {
    pthread_mutex_lock(&recursos);
    while(!cantTabaco || !cantPapel) {
      pthread_cond_wait(&recursoNuevo, &recursos);
    }
    cantTabaco--;
    cantPapel--;
    pthread_mutex_unlock(&recursos);
    fumar(1);
  }
}

void *fumador2(void *arg)
{
  for (;;) {
    pthread_mutex_lock(&recursos);
    while(!cantTabaco || !cantFosforos) {
      pthread_cond_wait(&recursoNuevo, &recursos);
    }
    cantTabaco--;
    cantFosforos--;
    pthread_mutex_unlock(&recursos);
    fumar(2);
  }
}

void *fumador3(void *arg)
{
  for (;;) {
    pthread_mutex_lock(&recursos);
    while(!cantPapel || !cantFosforos) {
      pthread_cond_wait(&recursoNuevo, &recursos);
    }
    cantPapel--;
    cantFosforos--;
    pthread_mutex_unlock(&recursos);
    fumar(3);
  }
}

// Este hilo consume recursos y aumenta el contador de recursos en la mesa
void *consumidor(void *arg){
  int producto = *((int*) arg);
  for (;;) {
    switch(producto) {
      case 0:
        sem_wait(&tabaco);
        pthread_mutex_lock(&recursos);
        cantTabaco++;
        pthread_mutex_unlock(&recursos);
        pthread_cond_broadcast(&recursoNuevo);
      break;
      case 1:
        sem_wait(&papel);
        pthread_mutex_lock(&recursos);
        cantPapel++;
        pthread_mutex_unlock(&recursos);
        pthread_cond_broadcast(&recursoNuevo);
      break;
      case 2:
        sem_wait(&fosforos);
        pthread_mutex_lock(&recursos);
        cantFosforos++;
        pthread_mutex_unlock(&recursos);
        pthread_cond_broadcast(&recursoNuevo);
      break;
    }
    sem_post(&otra_vez);
  }
}

int main()
{
    pthread_t s1, s2, s3, c1, c2, c3;
    int cero = 0, uno = 1, dos = 2;
    pthread_mutex_init(&recursos, NULL); 
    pthread_cond_init(&recursoNuevo, NULL);
    sem_init(&tabaco, 0, 0);
    sem_init(&papel, 0, 0);
    sem_init(&fosforos, 0, 0);
    sem_init(&otra_vez, 0, 1);
    pthread_create(&c1, NULL, consumidor, &cero);
    pthread_create(&c2, NULL, consumidor, &uno);
    pthread_create(&c3, NULL, consumidor, &dos);
    pthread_create(&s1, NULL, fumador1, NULL);
    pthread_create(&s2, NULL, fumador2, NULL);
    pthread_create(&s3, NULL, fumador3, NULL);
    agente();

    return 0;
}

