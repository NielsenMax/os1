# Ejercicio 4 

1. Por cada molinete entran N_VISITANTES personas, pero al ejecutar el programa es difı́cil que el resultado sea 2*N_VISITANTES. Explique a qué se debe esto.
 
Esto se debe a que se esta produciendo una condicion de carrera en la variable visitantes
Por lo tanto, un thread esta leyendo el valor de la variable, asumamos, N mientras que el otro
lee el mismo valor. Ahora, el primer thread se ejecuta M veces, de manera que escribre N+M en la variable,
y luego el segundo escribe N+1.
Por lo tanto, la suma del primer thread se pierde.

2. Ejecute el programa 5 veces con N_VISITANTES igual a 10. ¿El programa dio el resultado correcto siempre? Si esto es ası́, ¿por qué?

Cuando corrimos 5 veces con N_VISITANTES = 10, sumo correctamente, ya que para numeros chicos
la probabilidad de entrar en condicion de carrera es menor. Sin embargo, al correrlo 1000 veces,
obtuvimos un par de veces 19, es decir, entramos en codicion de carrera.

3. ¿Cuál es el mı́nimo valor que podrı́a imprimir el programa? ¿Bajo qué circustancia?
Cronologia de eventos:
- T1 lee un 0 en visitantes
- T2 lee un 0 en visitantes
- T1 escribe un 1 en visitantes
- T1 se ejecuta N_VISITANTES -2 veces correctamente
- T2 escribe un 1 en la variable visitantes
- T1 lee un 1 de la variable visitantes
- T2 se ejecuta N_VISITANTES -1 veces, hasta finalizar.
- T1 escribe un 2 en la variable visitantes, finalizando su ejecucion.
