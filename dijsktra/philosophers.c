#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

#define N_FILOSOFOS 5
#define ESPERA 5000000

pthread_mutex_t tenedor[N_FILOSOFOS];

void pensar(int i)
{
  printf("Filosofo %d pensando...\n",i);
  usleep(random() % ESPERA);
}

void comer(int i)
{
  printf("Filosofo %d comiendo...\n",i);
  usleep(random() % ESPERA);
}

void tomar_tenedores(int i)
{
  if(i){ /// el 0 es el zurdo
    pthread_mutex_lock(&tenedor[i]); /* Toma el tenedor a su derecha */
    pthread_mutex_lock(&tenedor[(i+1)%N_FILOSOFOS]); /* Toma el tenedor a su izquierda */
  }else{
    pthread_mutex_lock(&tenedor[(i+1)%N_FILOSOFOS]); /* Toma el tenedor a su izquierda */
    pthread_mutex_lock(&tenedor[i]); /* Toma el tenedor a su derecha */
  }
}

void dejar_tenedores(int i)
{
  pthread_mutex_unlock(&tenedor[i]); /* Deja el tenedor de su derecha */
  pthread_mutex_unlock(&tenedor[(i+1)%N_FILOSOFOS]); /* Deja el tenedor de su izquierda */
}

void *filosofo(void *arg)
{
  int i = (int)arg;
  for (;;)
  {
    tomar_tenedores(i);
    comer(i);
    dejar_tenedores(i);
    pensar(i);
  }
}

int main()
{
  int i;
  pthread_t filo[N_FILOSOFOS];
  for (i=0;i<N_FILOSOFOS;i++)
    pthread_mutex_init(&tenedor[i], NULL);
  for (i=0;i<N_FILOSOFOS;i++)
    pthread_create(&filo[i], NULL, filosofo, (void *)i);
  pthread_join(filo[0], NULL);
  return 0;
}

/*
1) Este programa puede terminar en deadlock. ¿En qué situación se puede
dar?
Si todos los filosofos toman su tenedor al mismo tiempo, bloquean a los demas.
De manera que nadie podra conseguir los 2 tenedores para comer.


2) Cansados de no comer los filósofos deciden pensar una solución a su pro-
blema. Uno razona que esto no sucederı́a si alguno de ellos fuese zurdo y
tome primero el tenedor de su izquierda.
Implemente esta solución y explique por qué funciona.

Asumamos que tenemos N (de 1 a N) filosofos en una mesa redonda, donde el filosofo N es el zurdo.
Supongamos que se forma una cadena de bloqueo empezando ṕor el filosofo 1, y terminando en el N-1
donde todos toman su tenedor a la derecha, salvo el zurdo.
Como el zurdo no tomo su tenedor a la derecha, ya que su tenedor izquierdo esta ocupado por el filosofo 1.
Entonces el Filosofo N-1, ya tiene el tenedor derecho y ahora puede tomar el tenedor izquierdo,
desbloqueando toda la cadena.

*/
