# Ejercicio 5
1. Este programa puede terminar en deadlock. ¿En qué situación se puede dar?
Si todos los filosofos toman su tenedor al mismo tiempo, bloquean a los demas.
De manera que nadie podra conseguir los 2 tenedores para comer.


2. Cansados de no comer los filósofos deciden pensar una solución a su problema. Uno razona que esto no sucederı́a si alguno de ellos fuese zurdo y tome primero el tenedor de su izquierda. Implemente esta solución y explique por qué funciona.

Asumamos que tenemos N (de 1 a N) filosofos en una mesa redonda, donde el filosofo N es el zurdo.
Supongamos que se forma una cadena de bloqueo empezando ṕor el filosofo 1, y terminando en el N-1
donde todos toman su tenedor a la derecha, salvo el zurdo.
Como el zurdo no tomo su tenedor a la derecha, ya que su tenedor izquierdo esta ocupado por el filosofo 1.
Entonces el Filosofo N-1, ya tiene el tenedor derecho y ahora puede tomar el tenedor izquierdo,
desbloqueando toda la cadena.

Implementado en el archivo philosofers.c

3. Otro filosofo piensa que tampoco tendrıan el problema si todos fuesen diestros pero solo comiesen a lo sumo N − 1 de ellos a la vez. Implemente esta solucion y explique por que funciona. Para ello va a necesitar un semaforo de Dijkstra.

Asumiendo el mismo orden que antes, los filosofos se enumeran del 1 a N. Sin perder generalidad,
asumamos que los primeros N - 1 filosofos toman el tenedor a su derecha, generando q esten todos bloqueados. 
El filosofo N no puede el tenedor a su derecha, es decir el de la izquierda del 1, 
ya que el semaforo lo prohibe. Ahora el primero puede tomar el tenedor a su izquierda y empezar a comer,
desbloqueando al segundo, y asi se podran ir desbloqueando todos
Implementado en filosofos_semaforos.c
