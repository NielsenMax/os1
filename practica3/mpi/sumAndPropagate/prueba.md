# Explicacion

La idea principal es realizar una solucion "bottom-up".
Dividimos el array en grupos de 2 procesos, los cuales se envian mutuamente
sus valores privados y lo suman a su cuenta. Ahora cada proceso de cada grupo
tiene como valor privado la suma de su grupo.
Ahora unimos los grupos de a 2, teniendo grupos de 4 procesos. En cada grupo,
el proceso 0 realizara el envio mutuo con el proceso 2 y el proceso 1 con el 3.
Ahora cada proceso de cada grupo tiene como valor interno la suma de su grupo.
Realizaremos esto hasta que el grupo tenga la longitud del array.

# Prueba del ejercicio de suma y consenso

Para probar que la solucion es robusta usaremos induccion sobre una cantidad
potencia de 2 de procesos, n=2^i:

Caso base i=0, n=2^0=1:
Si solo tenemos un proceso la suma y consenso es trivial.

Hipotesis inductiva:
Para i, es decir n=2^i procesos, la suma y conenso es correcta luego de i pasos.

Caso inductivo, i=i+1, n=2^(i+1):
Tenemos n=2^(i+1) procesos. Sabemos que luego de i pasos del algoritmo,
tenemos dos "grupos" de procesos {0,...,2^i-1} y {2^i,...,2^(i+1)} ambos de igual cantidad de elementos
y todos los procesos del mismo grupo tienen el mismo valor en su cuenta privada (la suma de su grupo),
esto es por Hipotesis inductiva.
Si ahora los procesos j y 2^i+j se envian mutuamente sus valores privados y lo suman a su cuenta, ambos
conoceran el valor de la suma total, con j entre 0 y 2^i-1.
Por lo tanto probamos que mientras la cantidad de procesos sea n=2^i, luego de i pasos se llegara al consenso.

