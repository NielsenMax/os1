#include <stdio.h>
#include <math.h>
#include "mpi.h"

int main(int argc, char** argv) {
  int rank, size;

  MPI_Status status;

  MPI_Init(&argc, &argv);
 
  MPI_Comm_rank(MPI_COMM_WORLD, &rank); // Rango del proceso
  MPI_Comm_size(MPI_COMM_WORLD, &size); // Numero total de procesos
  
  int p, side, partner, value, localSum = rank;
  int m = log(size)/log(2); // Log_2(size)
  for (int i = 0;i < m; i++) {
    p = pow(2,i);
    side = (rank/p) % 2;
    partner = rank+p*(-1)*(fabs(side - 0.5)/(side - 0.5)); // Si side es 1 resta p a rank, si es 0 lo suma
    MPI_Send(&localSum, 1, MPI_INT, partner, 0, MPI_COMM_WORLD); // Envio mi valor privado al proceso que me correspanda
    MPI_Recv(&value, 1, MPI_INT, partner, 0, MPI_COMM_WORLD, &status); // Recibo el valor privado del proceso que me corresponda
    localSum += value; // Actualizo mi valor privado
  }
  printf("Im %i and the total is %i \n", rank, localSum);
  MPI_Finalize();
  return 0;
}
