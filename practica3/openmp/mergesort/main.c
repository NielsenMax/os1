#include <stdlib.h>
#include <stdio.h>
#include "../timing.h"

void merge(int arr[], int l, int m, int r) {
  int i, j, k;
  int n1 = m - l + 1;
  int n2 =  r - m;
  
  // Arrays auxiliares
  int *L = malloc(n1 * sizeof(int)); 
  int *R = malloc(n2 * sizeof(int));
  
  // Copiamos del array principal al los secundarios
  for (i = 0; i < n1; i++)
    L[i] = arr[l + i];
  for (j = 0; j < n2; j++)
    R[j] = arr[m + 1+ j];

  // Unimos los arrays auxiliares al array principal arr[l..r]
  i = 0; 
  j = 0; 
  k = l; 
    while (i < n1 && j < n2)
    {
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }
  
    // Los siguientes ciclos son para copiar los elementos restantes de los subarrays
    // Solo se ejecuta uno de los dos
    while (i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }
  
    while (j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
  free(L);
  free(R);
}
// Mergesort pero secuencial
void mergesortSec(int arr[], int n) {
	if (n>1) {
		mergesortSec(&arr[0], n/2);
		mergesortSec(&arr[n/2], n - n/2);
		merge(arr, 0, (n/2)-1 , n-1);	
	}	
}

void mergesort(int arr[], int n) {
	if (n > 1) {
    // Si el array no es muy grande es mas rapido hacerlo secuencial
		if (n < 1000) {
			mergesortSec(&arr[0], n/2);
			mergesortSec(&arr[n/2], n - n/2);
		} else {
		# pragma omp parallel shared(arr)
		{
      # pragma omp single
      {
      // LLamamos a una task con la primera mitad
      # pragma omp task	shared(arr)	
      mergesort(&arr[0], n/2);
      // El thread actual sigue con la segunda mitad
      mergesort(&arr[n/2], n - n/2);
      }
		}
		}
    // Cuando terminan ambos llamamos al merge
		merge(arr, 0, n/2 - 1 , n-1);	
	}	
}
int isSorted(int arr[], int n) {
	for (int i = 1; i < n; i++) {
		if (arr[i-1] > arr[i]) {
			printf("The element %i: %i and the %i: %i are not sorted\n", i-1, arr[i-1], i, arr[i]);
      return 0;	
		}	
	}
	return 1;  	
}

#define N 100000000

int arr[N];
int arr2[N];
int main() {
  int aux;
	for (int i = 0; i<N ; i++) {
		aux = rand();
		arr[i] = aux;
    arr2[i] = aux;
	}
	TIME_void(mergesort(arr,N ), NULL);
  TIME_void(mergesortSec(arr2, N ), NULL);
  if (isSorted(arr, N)) {
    printf("The array is sorted\n");
  } else {
    printf("The array is not sorted\n");
  }
	return 0;	
}
