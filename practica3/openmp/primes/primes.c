#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <math.h>
#include "../timing.h"

int isPrime(long n) {
	int ret = 1; 
  long stop = ceil(sqrt(n));
	# pragma omp parallel
	{
	# pragma omp for
	for (int i = 2; i <= stop; i++) {
		if ( n%i ==0) {
			ret = 0;  // Hay una condicion de carrera benigna
		}
	}
	}
	return ret;	
}
int isPrimeSec(long n) {
    long stop = ceil(sqrt(n));
	for (int i = 2; i <= stop; i++) {
		if ( n%i == 0) {
			return  0;  // Al primer divisor corta
		}
	}
	return 1;
}
int main() {
	long n = 889651670422466179;
	int aux = TIME(isPrime(n), NULL) + TIME(isPrimeSec(n), NULL); 
	if (aux == 2) {
		printf("the number is prime");	
	} else if(aux == 1) {
		printf("El resultado no coincide");	
	} else {
		printf("the number is not prime");	
	} 
	return 0;
}
