#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_LINE 4096
#define MAX_ARGS 2048

char *aux(char *str, int l, int r) /// [)
{
  char *rta = malloc(r - l + 1);
  for (int i = l; i < r; i++)
  {
    rta[i - l] = str[i];
  }
  rta[r - l] = '\0';
  return rta;
}

char **extractCommand(char *input)
{
  char **list = malloc(sizeof(char *) * MAX_ARGS);
  int l = 0, r = 0, c = 0, i;
  for (i = 0; input[i] != '\0'; i++)
  {
    if (input[i] == ' ' || input[i] == '\n')
    {
      r = i;
      list[c] = aux(input, l, r);
      l = i + 1;
      c++;
    }
  }
  list[c++] = NULL;
  return list;
}

int main(int argc, char **argv){

  char input[MAX_LINE];
  char **args;
  int wstatus, i;
  
  printf(">> ");
  for (fgets(input, MAX_LINE, stdin); strcmp(input, "exit\n"); fgets(input, MAX_LINE, stdin)){

    int pipes = 0;
    args = extractCommand(input);

    int q = 0;
    while (args[q] != NULL && strcmp( args[q] , "|") != 0)
      q++;

    // Si estamos tenemos un pipe en el comando lo marcamos  
    if (args[q] != NULL && strcmp(args[q], "|") == 0){
      pipes++;
      free( args[q] );
      args[q] = NULL;
    }

    pid_t pid = fork();
    if (pid < 0){
      perror("Fork has failed\n");
    }else if (pid == 0){
      // Hijo
      if (pipes > 0){ // Si tenemos un pipe tenemos que forkear de nuevo 

        int pipefd[2];

        if (pipe(pipefd) == -1){
          perror("pipe");
          exit(EXIT_FAILURE);
        }

        pid_t phijo = fork();
        if (phijo < 0){
          perror("Fork has failed\n");
        }else if (phijo == 0){
          // Hijo del hijo
          close(1);
          dup(pipefd[1]);
          close(pipefd[0]);

          execvp(args[0], args);

        }else{
          // Hijo
          int status;

          close(0);       
          dup(pipefd[0]); 
          close(pipefd[1]);

          execvp(args[q + 1], args + q + 1);
        }
      }else{
        // Hijo, sin pipe
        execvp(args[0], args);
      }
    }
    wait(&wstatus);
    
    int i;
    
    for (i = 0; args[i] != NULL; i++)
      free(args[i]);

    if (pipes > 0){
      i++;
      for (; args[i] != NULL; i++)
        free( args[i] );
    }
    free(args);

    fflush(stdin);
    printf(">> ");
  }

  return 0;
}
