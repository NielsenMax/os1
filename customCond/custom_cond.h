#ifndef __CUSTOM_COND__
#define __CUSTOM_COND__
#include <pthread.h>
#include <semaphore.h>

struct cond {
  sem_t sem;
  pthread_mutex_t structLock;
  int waiting;
};

void cond_signal(struct cond *); 

void cond_broadcast(struct cond *); 

void cond_wait(struct cond *, pthread_mutex_t *); 

void cond_init(struct cond *); 

void cond_destroy(struct cond *); 

#endif // !__CUSTOM_COND__
