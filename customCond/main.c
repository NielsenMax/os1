#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

void cond_signal(struct cond *c) {
  pthread_mutex_lock(&c->structLock);
  if (c->waiting) {
    c->waiting--;
    sem_post(&c->sem);
  }
  pthread_mutex_unlock(&c->structLock);
}

void cond_broadcast(struct cond *c) {
  pthread_mutex_lock(&c->structLock);
  for (; c->waiting; c->waiting--) {
    sem_post(&c->sem);
  }
  pthread_mutex_unlock(&c->structLock);
}

void cond_wait(struct cond *c, pthread_mutex_t *l) {
  pthread_mutex_lock(&c->structLock);
  c->waiting++;
  pthread_mutex_unlock(&c->structLock);
  pthread_mutex_unlock(l);
  sem_wait(&c->sem);
  pthread_mutex_lock(l);
}

void cond_init(struct cond *c) {
  sem_init(&c->sem, 0, 0);
  pthread_mutex_init(&c->structLock, NULL);
  c->waiting = 0;
}

void cond_destroy(struct cond *c) {
  sem_destroy(&c->sem);
  pthread_mutex_destroy(&c->structLock);
}


#define M 5
#define N 5
#define ARRLEN 10240

struct rwmutex_t {
	sem_t writeSem;
  pthread_mutex_t structMutex;
  struct cond  noWriters;
	int readers;
  int writers;
};

struct rwmutex_t lock;

void rwmutex_init(struct rwmutex_t *l){
  pthread_mutex_init(&l->structMutex, NULL);
  cond_init(&l->noWriters);
  sem_init(&l->writeSem, 0, 1);
  l->readers = 0;
  l->writers = 0;
}

void rLock(struct rwmutex_t *l){
  pthread_mutex_lock(&l->structMutex);
	while (l->writers > 0) {
	  cond_wait(&l->noWriters, &l->structMutex);
	}
  if (l->readers == 0) {
    l->readers++;
    sem_wait(&l->writeSem);
  } else {
    l->readers++;
  }
  pthread_mutex_unlock(&l->structMutex);  
}

void rUnlock(struct rwmutex_t *l){
  pthread_mutex_lock(&l->structMutex);
  if(l->readers > 0){
    l->readers--;
    if(l->readers == 0){
      sem_post(&l->writeSem);
    }
  }  
  pthread_mutex_unlock(&l->structMutex);
}

void rwLock(struct rwmutex_t *l){
  pthread_mutex_lock(&l->structMutex);
  l->writers++;
  pthread_mutex_unlock(&l->structMutex);
  sem_wait(&l->writeSem);
}

void rwUnlock(struct rwmutex_t *l){
  pthread_mutex_lock(&l->structMutex);
  l->writers--;
  if (l->writers == 0) {
    cond_broadcast(&l->noWriters);
  }
  pthread_mutex_unlock(&l->structMutex);
  sem_post(&l->writeSem);  
}


int arr[ARRLEN];
void * escritor(void *arg){ 
	int i;
	int num = arg - (void*)0;
	while (1) {
    sleep(random() % 3);
    rwLock(&lock);
		printf("Escritor %d escribiendo\n", num);
    for (i = 0; i < ARRLEN; i++)
      arr[i] = num;
    rwUnlock(&lock);
	}
	return NULL;
}
void * lector(void *arg){
	int v, i;
	int num = arg - (void*)0;
	while (1) {
		sleep(random() % 3);
    rLock(&lock);
		v = arr[0];
		for (i = 1; i < ARRLEN; i++) {
			if (arr[i] != v)
				break;
		}
		if (i < ARRLEN)
			printf("Lector %d, error de lectura\n", num);
		else
			printf("Lector %d, dato %d\n", num, v);
    rUnlock(&lock);
	}
	return NULL;
}
int main(){
	pthread_t lectores[M], escritores[N];
  rwmutex_init(&lock);
	int i;
	for (i = 0; i < M; i++)
		pthread_create(&lectores[i], NULL, lector, i + (void*)0);
	for (i = 0; i < N; i++)
		pthread_create(&escritores[i], NULL, escritor, i + (void*)0);
	pthread_join(lectores[0], NULL);
	/* Espera para siempre */
	return 0;
}
