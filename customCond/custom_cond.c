#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include "custom_cond.h"

void cond_signal(struct cond *c) {
  pthread_mutex_lock(&c->structLock);
  if (c->waiting) {
    c->waiting--;
    sem_post(&c->sem);
  }
  pthread_mutex_unlock(&c->structLock);
}

void cond_broadcast(struct cond *c) {
  pthread_mutex_lock(&c->structLock);
  for (; c->waiting; c->waiting--) {
    sem_post(&c->sem);
  }
  pthread_mutex_unlock(&c->structLock);
}

void cond_wait(struct cond *c, pthread_mutex_t *l) {
  pthread_mutex_lock(&c->structLock);
  c->waiting++;
  pthread_mutex_unlock(&c->structLock);
  pthread_mutex_unlock(l);
  sem_wait(&c->sem);
  pthread_mutex_lock(l);
}

void cond_init(struct cond *c) {
  sem_init(&c->sem, 0, 0);
  pthread_mutex_init(&c->structLock, NULL);
  c->waiting = 0;
}

void cond_destroy(struct cond *c) {
  sem_destroy(&c->sem);
  pthread_mutex_destroy(&c->structLock);
}


